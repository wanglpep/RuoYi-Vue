import request from '@/utils/request'

// 查询软件程序使用记录列表
export function listRecord(query) {
  return request({
    url: '/acquisition/record/list',
    method: 'get',
    params: query
  })
}
// 查询软件程序使用记录列表
export function listHostInfo(query) {
  return request({
    url: '/acquisition/record/listHostInfo',
    method: 'get',
    params: query
  })
}

// 查询软件程序使用记录详细
export function getRecord(id) {
  return request({
    url: '/acquisition/record/' + id,
    method: 'get'
  })
}

// 新增软件程序使用记录
export function addRecord(data) {
  return request({
    url: '/acquisition/record',
    method: 'post',
    data: data
  })
}

// 修改软件程序使用记录
export function updateRecord(data) {
  return request({
    url: '/acquisition/record',
    method: 'put',
    data: data
  })
}

// 删除软件程序使用记录
export function delRecord(id) {
  return request({
    url: '/acquisition/record/' + id,
    method: 'delete'
  })
}
