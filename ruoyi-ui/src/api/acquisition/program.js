import request from '@/utils/request'

// 查询主机程序列表
export function listProgram(query) {
  return request({
    url: '/acquisition/program/list',
    method: 'get',
    params: query
  })
}

// 查询主机程序详细
export function getProgram(id) {
  return request({
    url: '/acquisition/program/' + id,
    method: 'get'
  })
}

// 新增主机程序
export function addProgram(data) {
  return request({
    url: '/acquisition/program',
    method: 'post',
    data: data
  })
}

// 修改主机程序
export function updateProgram(data) {
  return request({
    url: '/acquisition/program',
    method: 'put',
    data: data
  })
}

// 删除主机程序
export function delProgram(id) {
  return request({
    url: '/acquisition/program/' + id,
    method: 'delete'
  })
}
