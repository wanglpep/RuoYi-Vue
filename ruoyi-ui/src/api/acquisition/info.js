import request from '@/utils/request'

// 查询主机程序网卡信息列表
export function listInfo(query) {
  return request({
    url: '/acquisition/info/list',
    method: 'get',
    params: query
  })
}

// 查询主机程序网卡信息列表，携带有主机和程序信息
export function listHostInfo(query) {
  return request({
    url: '/acquisition/info/listHostInfo',
    method: 'get',
    params: query
  })
}

// 查询主机程序网卡信息详细
export function getInfo(id) {
  return request({
    url: '/acquisition/info/' + id,
    method: 'get'
  })
}

// 新增主机程序网卡信息
export function addInfo(data) {
  return request({
    url: '/acquisition/info',
    method: 'post',
    data: data
  })
}

// 修改主机程序网卡信息
export function updateInfo(data) {
  return request({
    url: '/acquisition/info',
    method: 'put',
    data: data
  })
}

// 删除主机程序网卡信息
export function delInfo(id) {
  return request({
    url: '/acquisition/info/' + id,
    method: 'delete'
  })
}
