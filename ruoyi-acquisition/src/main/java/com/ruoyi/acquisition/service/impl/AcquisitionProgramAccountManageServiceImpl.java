package com.ruoyi.acquisition.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.acquisition.mapper.AcquisitionProgramAccountManageMapper;
import com.ruoyi.acquisition.domain.AcquisitionProgramAccountManage;
import com.ruoyi.acquisition.service.IAcquisitionProgramAccountManageService;

/**
 * 软件账户管理Service业务层处理
 * 
 * @author abner
 * @date 2024-03-03
 */
@Service
public class AcquisitionProgramAccountManageServiceImpl implements IAcquisitionProgramAccountManageService 
{
    @Autowired
    private AcquisitionProgramAccountManageMapper acquisitionProgramAccountManageMapper;

    /**
     * 查询软件账户管理
     * 
     * @param id 软件账户管理主键
     * @return 软件账户管理
     */
    @Override
    public AcquisitionProgramAccountManage selectAcquisitionProgramAccountManageById(Long id)
    {
        return acquisitionProgramAccountManageMapper.selectAcquisitionProgramAccountManageById(id);
    }

    /**
     * 查询软件账户管理列表
     * 
     * @param acquisitionProgramAccountManage 软件账户管理
     * @return 软件账户管理
     */
    @Override
    public List<AcquisitionProgramAccountManage> selectAcquisitionProgramAccountManageList(AcquisitionProgramAccountManage acquisitionProgramAccountManage)
    {
        return acquisitionProgramAccountManageMapper.selectAcquisitionProgramAccountManageList(acquisitionProgramAccountManage);
    }

    /**
     * 新增软件账户管理
     * 
     * @param acquisitionProgramAccountManage 软件账户管理
     * @return 结果
     */
    @Override
    public int insertAcquisitionProgramAccountManage(AcquisitionProgramAccountManage acquisitionProgramAccountManage)
    {
        acquisitionProgramAccountManage.setCreateTime(DateUtils.getNowDate());
        return acquisitionProgramAccountManageMapper.insertAcquisitionProgramAccountManage(acquisitionProgramAccountManage);
    }

    /**
     * 修改软件账户管理
     * 
     * @param acquisitionProgramAccountManage 软件账户管理
     * @return 结果
     */
    @Override
    public int updateAcquisitionProgramAccountManage(AcquisitionProgramAccountManage acquisitionProgramAccountManage)
    {
        acquisitionProgramAccountManage.setUpdateTime(DateUtils.getNowDate());
        return acquisitionProgramAccountManageMapper.updateAcquisitionProgramAccountManage(acquisitionProgramAccountManage);
    }

    /**
     * 批量删除软件账户管理
     * 
     * @param ids 需要删除的软件账户管理主键
     * @return 结果
     */
    @Override
    public int deleteAcquisitionProgramAccountManageByIds(Long[] ids)
    {
        return acquisitionProgramAccountManageMapper.deleteAcquisitionProgramAccountManageByIds(ids);
    }

    /**
     * 删除软件账户管理信息
     * 
     * @param id 软件账户管理主键
     * @return 结果
     */
    @Override
    public int deleteAcquisitionProgramAccountManageById(Long id)
    {
        return acquisitionProgramAccountManageMapper.deleteAcquisitionProgramAccountManageById(id);
    }
}
