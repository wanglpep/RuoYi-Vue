package com.ruoyi.acquisition.service.impl;

import com.ruoyi.acquisition.domain.AcquisitionHostProgram;
import com.ruoyi.acquisition.mapper.AcquisitionHostProgramMapper;
import com.ruoyi.acquisition.service.IAcquisitionHostProgramService;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 主机程序Service业务层处理
 *
 * @author abner
 * @date 2024-03-02
 */
@Service
public class AcquisitionHostProgramServiceImpl implements IAcquisitionHostProgramService
{
    @Autowired
    private AcquisitionHostProgramMapper acquisitionHostProgramMapper;

    /**
     * 查询主机程序
     *
     * @param id 主机程序主键
     * @return 主机程序
     */
    @Override
    public AcquisitionHostProgram selectAcquisitionHostProgramById(Long id)
    {
        return acquisitionHostProgramMapper.selectAcquisitionHostProgramById(id);
    }

    /**
     * 查询主机程序列表
     *
     * @param acquisitionHostProgram 主机程序
     * @return 主机程序
     */
    @Override
    public List<AcquisitionHostProgram> selectAcquisitionHostProgramList(AcquisitionHostProgram acquisitionHostProgram)
    {
        return acquisitionHostProgramMapper.selectAcquisitionHostProgramList(acquisitionHostProgram);
    }

    /**
     * 新增主机程序
     *
     * @param acquisitionHostProgram 主机程序
     * @return 结果
     */
    @Override
    public int insertAcquisitionHostProgram(AcquisitionHostProgram acquisitionHostProgram)
    {
        acquisitionHostProgram.setCreateTime(DateUtils.getNowDate());
        return acquisitionHostProgramMapper.insertAcquisitionHostProgram(acquisitionHostProgram);
    }

    /**
     * 修改主机程序
     *
     * @param acquisitionHostProgram 主机程序
     * @return 结果
     */
    @Override
    public int updateAcquisitionHostProgram(AcquisitionHostProgram acquisitionHostProgram)
    {
        acquisitionHostProgram.setUpdateTime(DateUtils.getNowDate());
        return acquisitionHostProgramMapper.updateAcquisitionHostProgram(acquisitionHostProgram);
    }

    /**
     * 批量删除主机程序
     *
     * @param ids 需要删除的主机程序主键
     * @return 结果
     */
    @Override
    public int deleteAcquisitionHostProgramByIds(Long[] ids)
    {
        return acquisitionHostProgramMapper.deleteAcquisitionHostProgramByIds(ids);
    }

    /**
     * 删除主机程序信息
     *
     * @param id 主机程序主键
     * @return 结果
     */
    @Override
    public int deleteAcquisitionHostProgramById(Long id)
    {
        return acquisitionHostProgramMapper.deleteAcquisitionHostProgramById(id);
    }
}
