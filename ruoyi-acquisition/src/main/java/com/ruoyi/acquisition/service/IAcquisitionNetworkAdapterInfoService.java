package com.ruoyi.acquisition.service;

import java.util.List;
import com.ruoyi.acquisition.domain.AcquisitionNetworkAdapterInfo;
import com.ruoyi.acquisition.domain.vo.AcquisitionNetworkAdapterInfoVo;

/**
 * 主机程序网卡信息Service接口
 *
 * @author abner
 * @date 2024-03-02
 */
public interface IAcquisitionNetworkAdapterInfoService
{
    /**
     * 查询主机程序网卡信息
     *
     * @param id 主机程序网卡信息主键
     * @return 主机程序网卡信息
     */
    public AcquisitionNetworkAdapterInfo selectAcquisitionNetworkAdapterInfoById(Long id);

    /**
     * 查询主机程序网卡信息列表
     *
     * @param acquisitionNetworkAdapterInfo 主机程序网卡信息
     * @return 主机程序网卡信息集合
     */
    public List<AcquisitionNetworkAdapterInfo> selectAcquisitionNetworkAdapterInfoList(AcquisitionNetworkAdapterInfo acquisitionNetworkAdapterInfo);

    /**
     * 新增主机程序网卡信息
     *
     * @param acquisitionNetworkAdapterInfo 主机程序网卡信息
     * @return 结果
     */
    public int insertAcquisitionNetworkAdapterInfo(AcquisitionNetworkAdapterInfo acquisitionNetworkAdapterInfo);

    /**
     * 修改主机程序网卡信息
     *
     * @param acquisitionNetworkAdapterInfo 主机程序网卡信息
     * @return 结果
     */
    public int updateAcquisitionNetworkAdapterInfo(AcquisitionNetworkAdapterInfo acquisitionNetworkAdapterInfo);

    /**
     * 批量删除主机程序网卡信息
     *
     * @param ids 需要删除的主机程序网卡信息主键集合
     * @return 结果
     */
    public int deleteAcquisitionNetworkAdapterInfoByIds(Long[] ids);

    /**
     * 删除主机程序网卡信息信息
     *
     * @param id 主机程序网卡信息主键
     * @return 结果
     */
    public int deleteAcquisitionNetworkAdapterInfoById(Long id);

    /**
     * 查询主机程序网卡信息列表
     *
     * @param acquisitionNetworkAdapterInfo 主机程序网卡信息
     * @return 主机程序网卡信息集合
     */
    public List<AcquisitionNetworkAdapterInfoVo> selectAcquisitionNetworkAdapterInfoVoList(AcquisitionNetworkAdapterInfoVo acquisitionNetworkAdapterInfo);
}
