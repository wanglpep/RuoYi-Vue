package com.ruoyi.acquisition.service.impl;

import com.ruoyi.acquisition.domain.AcquisitionNetworkAdapterInfo;
import com.ruoyi.acquisition.domain.vo.AcquisitionNetworkAdapterInfoVo;
import com.ruoyi.acquisition.mapper.AcquisitionNetworkAdapterInfoMapper;
import com.ruoyi.acquisition.service.IAcquisitionNetworkAdapterInfoService;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 主机程序网卡信息Service业务层处理
 *
 * @author abner
 * @date 2024-03-02
 */
@Service
public class AcquisitionNetworkAdapterInfoServiceImpl implements IAcquisitionNetworkAdapterInfoService
{
    @Autowired
    private AcquisitionNetworkAdapterInfoMapper acquisitionNetworkAdapterInfoMapper;

    /**
     * 查询主机程序网卡信息
     *
     * @param id 主机程序网卡信息主键
     * @return 主机程序网卡信息
     */
    @Override
    public AcquisitionNetworkAdapterInfo selectAcquisitionNetworkAdapterInfoById(Long id)
    {
        return acquisitionNetworkAdapterInfoMapper.selectAcquisitionNetworkAdapterInfoById(id);
    }

    /**
     * 查询主机程序网卡信息列表
     *
     * @param acquisitionNetworkAdapterInfo 主机程序网卡信息
     * @return 主机程序网卡信息
     */
    @Override
    public List<AcquisitionNetworkAdapterInfo> selectAcquisitionNetworkAdapterInfoList(AcquisitionNetworkAdapterInfo acquisitionNetworkAdapterInfo)
    {
        return acquisitionNetworkAdapterInfoMapper.selectAcquisitionNetworkAdapterInfoList(acquisitionNetworkAdapterInfo);
    }

    /**
     * 新增主机程序网卡信息
     *
     * @param acquisitionNetworkAdapterInfo 主机程序网卡信息
     * @return 结果
     */
    @Override
    public int insertAcquisitionNetworkAdapterInfo(AcquisitionNetworkAdapterInfo acquisitionNetworkAdapterInfo)
    {
        acquisitionNetworkAdapterInfo.setCreateTime(DateUtils.getNowDate());
        return acquisitionNetworkAdapterInfoMapper.insertAcquisitionNetworkAdapterInfo(acquisitionNetworkAdapterInfo);
    }

    /**
     * 修改主机程序网卡信息
     *
     * @param acquisitionNetworkAdapterInfo 主机程序网卡信息
     * @return 结果
     */
    @Override
    public int updateAcquisitionNetworkAdapterInfo(AcquisitionNetworkAdapterInfo acquisitionNetworkAdapterInfo)
    {
        acquisitionNetworkAdapterInfo.setUpdateTime(DateUtils.getNowDate());
        return acquisitionNetworkAdapterInfoMapper.updateAcquisitionNetworkAdapterInfo(acquisitionNetworkAdapterInfo);
    }

    /**
     * 批量删除主机程序网卡信息
     *
     * @param ids 需要删除的主机程序网卡信息主键
     * @return 结果
     */
    @Override
    public int deleteAcquisitionNetworkAdapterInfoByIds(Long[] ids)
    {
        return acquisitionNetworkAdapterInfoMapper.deleteAcquisitionNetworkAdapterInfoByIds(ids);
    }

    /**
     * 删除主机程序网卡信息信息
     *
     * @param id 主机程序网卡信息主键
     * @return 结果
     */
    @Override
    public int deleteAcquisitionNetworkAdapterInfoById(Long id)
    {
        return acquisitionNetworkAdapterInfoMapper.deleteAcquisitionNetworkAdapterInfoById(id);
    }

    /**
     * 查询主机程序网卡信息列表
     *
     * @param acquisitionNetworkAdapterInfoVo 主机程序网卡信息
     * @return 主机程序网卡信息
     */
    @Override
    public List<AcquisitionNetworkAdapterInfoVo> selectAcquisitionNetworkAdapterInfoVoList(AcquisitionNetworkAdapterInfoVo acquisitionNetworkAdapterInfoVo)
    {
        return acquisitionNetworkAdapterInfoMapper.selectAcquisitionNetworkAdapterInfoVoList(acquisitionNetworkAdapterInfoVo);
    }
}
