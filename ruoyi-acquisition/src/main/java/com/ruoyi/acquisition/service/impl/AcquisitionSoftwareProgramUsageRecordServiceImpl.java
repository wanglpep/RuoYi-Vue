package com.ruoyi.acquisition.service.impl;

import java.util.List;

import com.ruoyi.acquisition.domain.vo.AcquisitionSoftwareProgramUsageRecordVo;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.acquisition.mapper.AcquisitionSoftwareProgramUsageRecordMapper;
import com.ruoyi.acquisition.domain.AcquisitionSoftwareProgramUsageRecord;
import com.ruoyi.acquisition.service.IAcquisitionSoftwareProgramUsageRecordService;

/**
 * 软件程序使用记录Service业务层处理
 *
 * @author abner
 * @date 2024-03-02
 */
@Service
public class AcquisitionSoftwareProgramUsageRecordServiceImpl implements IAcquisitionSoftwareProgramUsageRecordService
{
    @Autowired
    private AcquisitionSoftwareProgramUsageRecordMapper acquisitionSoftwareProgramUsageRecordMapper;

    /**
     * 查询软件程序使用记录
     *
     * @param id 软件程序使用记录主键
     * @return 软件程序使用记录
     */
    @Override
    public AcquisitionSoftwareProgramUsageRecord selectAcquisitionSoftwareProgramUsageRecordById(Long id)
    {
        return acquisitionSoftwareProgramUsageRecordMapper.selectAcquisitionSoftwareProgramUsageRecordById(id);
    }

    /**
     * 查询软件程序使用记录列表
     *
     * @param acquisitionSoftwareProgramUsageRecord 软件程序使用记录
     * @return 软件程序使用记录
     */
    @Override
    public List<AcquisitionSoftwareProgramUsageRecord> selectAcquisitionSoftwareProgramUsageRecordList(AcquisitionSoftwareProgramUsageRecord acquisitionSoftwareProgramUsageRecord)
    {
        return acquisitionSoftwareProgramUsageRecordMapper.selectAcquisitionSoftwareProgramUsageRecordList(acquisitionSoftwareProgramUsageRecord);
    }

    /**
     * 新增软件程序使用记录
     *
     * @param acquisitionSoftwareProgramUsageRecord 软件程序使用记录
     * @return 结果
     */
    @Override
    public int insertAcquisitionSoftwareProgramUsageRecord(AcquisitionSoftwareProgramUsageRecord acquisitionSoftwareProgramUsageRecord)
    {
        acquisitionSoftwareProgramUsageRecord.setCreateTime(DateUtils.getNowDate());
        return acquisitionSoftwareProgramUsageRecordMapper.insertAcquisitionSoftwareProgramUsageRecord(acquisitionSoftwareProgramUsageRecord);
    }

    /**
     * 修改软件程序使用记录
     *
     * @param acquisitionSoftwareProgramUsageRecord 软件程序使用记录
     * @return 结果
     */
    @Override
    public int updateAcquisitionSoftwareProgramUsageRecord(AcquisitionSoftwareProgramUsageRecord acquisitionSoftwareProgramUsageRecord)
    {
        acquisitionSoftwareProgramUsageRecord.setUpdateTime(DateUtils.getNowDate());
        return acquisitionSoftwareProgramUsageRecordMapper.updateAcquisitionSoftwareProgramUsageRecord(acquisitionSoftwareProgramUsageRecord);
    }

    /**
     * 批量删除软件程序使用记录
     *
     * @param ids 需要删除的软件程序使用记录主键
     * @return 结果
     */
    @Override
    public int deleteAcquisitionSoftwareProgramUsageRecordByIds(Long[] ids)
    {
        return acquisitionSoftwareProgramUsageRecordMapper.deleteAcquisitionSoftwareProgramUsageRecordByIds(ids);
    }

    /**
     * 删除软件程序使用记录信息
     *
     * @param id 软件程序使用记录主键
     * @return 结果
     */
    @Override
    public int deleteAcquisitionSoftwareProgramUsageRecordById(Long id)
    {
        return acquisitionSoftwareProgramUsageRecordMapper.deleteAcquisitionSoftwareProgramUsageRecordById(id);
    }

    /**
     * 查询软件程序使用记录列表
     *
     * @param acquisitionSoftwareProgramUsageRecord 软件程序使用记录
     * @return 软件程序使用记录
     */
    @Override
    public List<AcquisitionSoftwareProgramUsageRecordVo> selectAcquisitionSoftwareProgramUsageRecordVoList(AcquisitionSoftwareProgramUsageRecordVo acquisitionSoftwareProgramUsageRecord) {
        return acquisitionSoftwareProgramUsageRecordMapper.selectAcquisitionSoftwareProgramUsageRecordVoList(acquisitionSoftwareProgramUsageRecord);
    }
}
