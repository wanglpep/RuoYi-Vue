package com.ruoyi.acquisition.service;

import java.util.List;
import com.ruoyi.acquisition.domain.AcquisitionProgramAccountManage;

/**
 * 软件账户管理Service接口
 * 
 * @author abner
 * @date 2024-03-03
 */
public interface IAcquisitionProgramAccountManageService 
{
    /**
     * 查询软件账户管理
     * 
     * @param id 软件账户管理主键
     * @return 软件账户管理
     */
    public AcquisitionProgramAccountManage selectAcquisitionProgramAccountManageById(Long id);

    /**
     * 查询软件账户管理列表
     * 
     * @param acquisitionProgramAccountManage 软件账户管理
     * @return 软件账户管理集合
     */
    public List<AcquisitionProgramAccountManage> selectAcquisitionProgramAccountManageList(AcquisitionProgramAccountManage acquisitionProgramAccountManage);

    /**
     * 新增软件账户管理
     * 
     * @param acquisitionProgramAccountManage 软件账户管理
     * @return 结果
     */
    public int insertAcquisitionProgramAccountManage(AcquisitionProgramAccountManage acquisitionProgramAccountManage);

    /**
     * 修改软件账户管理
     * 
     * @param acquisitionProgramAccountManage 软件账户管理
     * @return 结果
     */
    public int updateAcquisitionProgramAccountManage(AcquisitionProgramAccountManage acquisitionProgramAccountManage);

    /**
     * 批量删除软件账户管理
     * 
     * @param ids 需要删除的软件账户管理主键集合
     * @return 结果
     */
    public int deleteAcquisitionProgramAccountManageByIds(Long[] ids);

    /**
     * 删除软件账户管理信息
     * 
     * @param id 软件账户管理主键
     * @return 结果
     */
    public int deleteAcquisitionProgramAccountManageById(Long id);
}
