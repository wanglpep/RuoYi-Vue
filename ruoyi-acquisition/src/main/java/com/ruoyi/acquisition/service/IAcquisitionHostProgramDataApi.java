package com.ruoyi.acquisition.service;

import com.ruoyi.acquisition.config.InterfaceParamsBean;
import com.ruoyi.acquisition.domain.AcquisitionProgramAccountManage;
import com.ruoyi.acquisition.domain.vo.ApiAcquisitionHostProgramVo;
import com.ruoyi.common.core.domain.AjaxResult;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @author wanglp
 * @create 2024/3/2 23:41
 */
public interface IAcquisitionHostProgramDataApi {
    AjaxResult checkReceiveParams(Map<String, List<ApiAcquisitionHostProgramVo>> acquisitionHostProgramMap, HttpServletRequest request, InterfaceParamsBean hostProgramInfo);

    AjaxResult insertAcquisitionHostProgramVo(List<ApiAcquisitionHostProgramVo> apiAcquisitionHostProgramVos);

    AjaxResult checkReceiveParamsAccountManage(AcquisitionProgramAccountManage accountManage, HttpServletRequest request, InterfaceParamsBean accountManageConfig);
}
