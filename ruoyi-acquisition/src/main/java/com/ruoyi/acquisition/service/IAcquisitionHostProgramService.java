package com.ruoyi.acquisition.service;

import com.ruoyi.acquisition.domain.AcquisitionHostProgram;

import java.util.List;

/**
 * 主机程序Service接口
 *
 * @author abner
 * @date 2024-03-02
 */
public interface IAcquisitionHostProgramService
{
    /**
     * 查询主机程序
     *
     * @param id 主机程序主键
     * @return 主机程序
     */
    public AcquisitionHostProgram selectAcquisitionHostProgramById(Long id);

    /**
     * 查询主机程序列表
     *
     * @param acquisitionHostProgram 主机程序
     * @return 主机程序集合
     */
    public List<AcquisitionHostProgram> selectAcquisitionHostProgramList(AcquisitionHostProgram acquisitionHostProgram);

    /**
     * 新增主机程序
     *
     * @param acquisitionHostProgram 主机程序
     * @return 结果
     */
    public int insertAcquisitionHostProgram(AcquisitionHostProgram acquisitionHostProgram);

    /**
     * 修改主机程序
     *
     * @param acquisitionHostProgram 主机程序
     * @return 结果
     */
    public int updateAcquisitionHostProgram(AcquisitionHostProgram acquisitionHostProgram);

    /**
     * 批量删除主机程序
     *
     * @param ids 需要删除的主机程序主键集合
     * @return 结果
     */
    public int deleteAcquisitionHostProgramByIds(Long[] ids);

    /**
     * 删除主机程序信息
     *
     * @param id 主机程序主键
     * @return 结果
     */
    public int deleteAcquisitionHostProgramById(Long id);
}
