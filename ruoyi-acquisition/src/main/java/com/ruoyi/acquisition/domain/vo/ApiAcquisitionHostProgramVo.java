package com.ruoyi.acquisition.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * 接收程序信息对象
 * @author wanglp
 * @create 2024/3/2 23:33
 */
@Data
public class ApiAcquisitionHostProgramVo {
    private static final long serialVersionUID = 1L;

    /** 唯一标识符 */
    private Long id;

    /** 主机名称 */
    private String hostName;

    /** 系统版本 */
    private String systemVersion;

    /** 程序路径 */
    private String programPath;

    /** 程序名称 */
    private String programName;

    /** 程序文件版本 */
    private String programFileVersion;

    /** 程序产品版本 */
    private String programProductVersion;

    /** 程序启动时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date programStartTime;

    /** 最后运行时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date lastRunTime;

    /** 登录账号 */
    private String loginAccount;

    /** 登录时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date loginTime;

    /** 进程PID */
    private String processPid;

    /**
     * 网卡信息
     */
    List<ApiAcquisitionNetworkAdapterInfoVo> networkAdapterInfoList;
}
