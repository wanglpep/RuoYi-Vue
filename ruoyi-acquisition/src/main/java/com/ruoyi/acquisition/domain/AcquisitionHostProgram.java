package com.ruoyi.acquisition.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 主机程序对象 acquisition_host_program
 *
 * @author abner
 * @date 2024-03-02
 */
public class AcquisitionHostProgram extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 唯一标识符 */
    private Long id;

    /** 主机名称 */
    @Excel(name = "主机名称")
    private String hostName;

    /** 系统版本 */
    @Excel(name = "系统版本")
    private String systemVersion;

    /** 程序路径 */
    @Excel(name = "程序路径")
    private String programPath;

    /** 程序名称 */
    @Excel(name = "程序名称")
    private String programName;

    /** 程序文件版本 */
    @Excel(name = "程序文件版本")
    private String programFileVersion;

    /** 程序产品版本 */
    @Excel(name = "程序产品版本")
    private String programProductVersion;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setHostName(String hostName)
    {
        this.hostName = hostName;
    }

    public String getHostName()
    {
        return hostName;
    }
    public void setSystemVersion(String systemVersion)
    {
        this.systemVersion = systemVersion;
    }

    public String getSystemVersion()
    {
        return systemVersion;
    }
    public void setProgramPath(String programPath)
    {
        this.programPath = programPath;
    }

    public String getProgramPath()
    {
        return programPath;
    }
    public void setProgramName(String programName)
    {
        this.programName = programName;
    }

    public String getProgramName()
    {
        return programName;
    }
    public void setProgramFileVersion(String programFileVersion)
    {
        this.programFileVersion = programFileVersion;
    }

    public String getProgramFileVersion()
    {
        return programFileVersion;
    }
    public void setProgramProductVersion(String programProductVersion)
    {
        this.programProductVersion = programProductVersion;
    }

    public String getProgramProductVersion()
    {
        return programProductVersion;
    }

    public AcquisitionHostProgram(String hostName, String programName, String programFileVersion, String programProductVersion) {
        this.hostName = hostName;
        this.programName = programName;
        this.programFileVersion = programFileVersion;
        this.programProductVersion = programProductVersion;
    }

    public AcquisitionHostProgram() {
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("hostName", getHostName())
            .append("systemVersion", getSystemVersion())
            .append("programPath", getProgramPath())
            .append("programName", getProgramName())
            .append("programFileVersion", getProgramFileVersion())
            .append("programProductVersion", getProgramProductVersion())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
