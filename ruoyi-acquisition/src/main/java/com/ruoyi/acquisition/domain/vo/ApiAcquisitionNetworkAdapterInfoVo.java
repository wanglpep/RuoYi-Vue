package com.ruoyi.acquisition.domain.vo;

import lombok.Data;

/**
 * @author wanglp
 * @create 2024/3/2 23:37
 */
@Data
public class ApiAcquisitionNetworkAdapterInfoVo {

    /** 唯一标识符 */
    private Long id;

    /** 软件程序使用记录表ID */
    private Long programUsageRecordId;

    /** 主机程序表ID */
    private Long hostProgramId;

    /** MAC地址 */
    private String macAddress;

    /** IP地址 */
    private String ipAddress;
}
