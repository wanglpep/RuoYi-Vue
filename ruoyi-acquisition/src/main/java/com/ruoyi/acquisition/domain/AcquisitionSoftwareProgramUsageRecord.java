package com.ruoyi.acquisition.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 软件程序使用记录对象 acquisition_software_program_usage_record
 *
 * @author abner
 * @date 2024-03-02
 */
public class AcquisitionSoftwareProgramUsageRecord extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 唯一标识符 */
    private Long id;

    /** 主机程序表ID */
    @Excel(name = "主机程序表ID")
    private Long hostProgramId;

    /** 程序启动时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "程序启动时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date programStartTime;

    /** 最后运行时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "最后运行时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date lastRunTime;

    /** 登录账号 */
    @Excel(name = "登录账号")
    private String loginAccount;

    /** 登录时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "登录时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date loginTime;

    /** 进程PID */
    @Excel(name = "进程PID")
    private String processPid;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setHostProgramId(Long hostProgramId)
    {
        this.hostProgramId = hostProgramId;
    }

    public Long getHostProgramId()
    {
        return hostProgramId;
    }
    public void setProgramStartTime(Date programStartTime)
    {
        this.programStartTime = programStartTime;
    }

    public Date getProgramStartTime()
    {
        return programStartTime;
    }
    public void setLastRunTime(Date lastRunTime)
    {
        this.lastRunTime = lastRunTime;
    }

    public Date getLastRunTime()
    {
        return lastRunTime;
    }
    public void setLoginAccount(String loginAccount)
    {
        this.loginAccount = loginAccount;
    }

    public String getLoginAccount()
    {
        return loginAccount;
    }
    public void setLoginTime(Date loginTime)
    {
        this.loginTime = loginTime;
    }

    public Date getLoginTime()
    {
        return loginTime;
    }
    public void setProcessPid(String processPid)
    {
        this.processPid = processPid;
    }

    public String getProcessPid()
    {
        return processPid;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("hostProgramId", getHostProgramId())
            .append("programStartTime", getProgramStartTime())
            .append("lastRunTime", getLastRunTime())
            .append("loginAccount", getLoginAccount())
            .append("loginTime", getLoginTime())
            .append("processPid", getProcessPid())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
