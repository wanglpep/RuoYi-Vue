package com.ruoyi.acquisition.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 软件账户管理对象 acquisition_program_account_manage
 * 
 * @author abner
 * @date 2024-03-03
 */
public class AcquisitionProgramAccountManage extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 唯一标识符 */
    private Long id;

    /** 程序名称 */
    @Excel(name = "程序名称")
    private String programName;

    /** 登录账号 */
    @Excel(name = "登录账号")
    private String loginAccount;

    /** 账号密码 */
    @Excel(name = "账号密码")
    private String accountPassword;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setProgramName(String programName) 
    {
        this.programName = programName;
    }

    public String getProgramName() 
    {
        return programName;
    }
    public void setLoginAccount(String loginAccount) 
    {
        this.loginAccount = loginAccount;
    }

    public String getLoginAccount() 
    {
        return loginAccount;
    }
    public void setAccountPassword(String accountPassword) 
    {
        this.accountPassword = accountPassword;
    }

    public String getAccountPassword() 
    {
        return accountPassword;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("programName", getProgramName())
            .append("loginAccount", getLoginAccount())
            .append("accountPassword", getAccountPassword())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
