package com.ruoyi.acquisition.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 主机程序网卡信息对象 acquisition_network_adapter_info
 * 
 * @author abner
 * @date 2024-03-02
 */
public class AcquisitionNetworkAdapterInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 唯一标识符 */
    private Long id;

    /** 软件程序使用记录表ID */
    @Excel(name = "软件程序使用记录表ID")
    private Long programUsageRecordId;

    /** 主机程序表ID */
    @Excel(name = "主机程序表ID")
    private Long hostProgramId;

    /** MAC地址 */
    @Excel(name = "MAC地址")
    private String macAddress;

    /** IP地址 */
    @Excel(name = "IP地址")
    private String ipAddress;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setProgramUsageRecordId(Long programUsageRecordId) 
    {
        this.programUsageRecordId = programUsageRecordId;
    }

    public Long getProgramUsageRecordId() 
    {
        return programUsageRecordId;
    }
    public void setHostProgramId(Long hostProgramId) 
    {
        this.hostProgramId = hostProgramId;
    }

    public Long getHostProgramId() 
    {
        return hostProgramId;
    }
    public void setMacAddress(String macAddress) 
    {
        this.macAddress = macAddress;
    }

    public String getMacAddress() 
    {
        return macAddress;
    }
    public void setIpAddress(String ipAddress) 
    {
        this.ipAddress = ipAddress;
    }

    public String getIpAddress() 
    {
        return ipAddress;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("programUsageRecordId", getProgramUsageRecordId())
            .append("hostProgramId", getHostProgramId())
            .append("macAddress", getMacAddress())
            .append("ipAddress", getIpAddress())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
