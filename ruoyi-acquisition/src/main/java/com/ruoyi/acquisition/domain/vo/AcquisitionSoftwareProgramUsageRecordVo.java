package com.ruoyi.acquisition.domain.vo;

import com.ruoyi.acquisition.domain.AcquisitionSoftwareProgramUsageRecord;
import com.ruoyi.common.annotation.Excel;

/**
 * 主机程序使用记录携带主机和程序信息
 * @author abner
 * @create 2024/3/2 21:04
 */
public class AcquisitionSoftwareProgramUsageRecordVo extends AcquisitionSoftwareProgramUsageRecord {
    private static final long serialVersionUID = 1L;
    /** 主机名称 */
    @Excel(name = "主机名称")
    private String hostName;

    /** 程序名称 */
    @Excel(name = "程序名称")
    private String programName;

    /** 程序文件版本 */
    @Excel(name = "程序文件版本")
    private String programFileVersion;

    /** 程序产品版本 */
    @Excel(name = "程序产品版本")
    private String programProductVersion;

    /** 系统版本 */
    @Excel(name = "系统版本")
    private String systemVersion;

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public String getProgramName() {
        return programName;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }

    public String getProgramFileVersion() {
        return programFileVersion;
    }

    public void setProgramFileVersion(String programFileVersion) {
        this.programFileVersion = programFileVersion;
    }

    public String getProgramProductVersion() {
        return programProductVersion;
    }

    public void setProgramProductVersion(String programProductVersion) {
        this.programProductVersion = programProductVersion;
    }

    public String getSystemVersion() {
        return systemVersion;
    }

    public void setSystemVersion(String systemVersion) {
        this.systemVersion = systemVersion;
    }
}
