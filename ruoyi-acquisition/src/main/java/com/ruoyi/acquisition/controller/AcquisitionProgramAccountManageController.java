package com.ruoyi.acquisition.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.acquisition.domain.AcquisitionProgramAccountManage;
import com.ruoyi.acquisition.service.IAcquisitionProgramAccountManageService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 软件账户管理Controller
 * 
 * @author abner
 * @date 2024-03-03
 */
@RestController
@RequestMapping("/acquisition/accountManage")
public class AcquisitionProgramAccountManageController extends BaseController
{
    @Autowired
    private IAcquisitionProgramAccountManageService acquisitionProgramAccountManageService;

    /**
     * 查询软件账户管理列表
     */
    @PreAuthorize("@ss.hasPermi('acquisition:accountManage:list')")
    @GetMapping("/list")
    public TableDataInfo list(AcquisitionProgramAccountManage acquisitionProgramAccountManage)
    {
        startPage();
        List<AcquisitionProgramAccountManage> list = acquisitionProgramAccountManageService.selectAcquisitionProgramAccountManageList(acquisitionProgramAccountManage);
        return getDataTable(list);
    }

    /**
     * 导出软件账户管理列表
     */
    @PreAuthorize("@ss.hasPermi('acquisition:accountManage:export')")
    @Log(title = "软件账户管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, AcquisitionProgramAccountManage acquisitionProgramAccountManage)
    {
        List<AcquisitionProgramAccountManage> list = acquisitionProgramAccountManageService.selectAcquisitionProgramAccountManageList(acquisitionProgramAccountManage);
        ExcelUtil<AcquisitionProgramAccountManage> util = new ExcelUtil<AcquisitionProgramAccountManage>(AcquisitionProgramAccountManage.class);
        util.exportExcel(response, list, "软件账户管理数据");
    }

    /**
     * 获取软件账户管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('acquisition:accountManage:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(acquisitionProgramAccountManageService.selectAcquisitionProgramAccountManageById(id));
    }

    /**
     * 新增软件账户管理
     */
    @PreAuthorize("@ss.hasPermi('acquisition:accountManage:add')")
    @Log(title = "软件账户管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AcquisitionProgramAccountManage acquisitionProgramAccountManage)
    {
        return toAjax(acquisitionProgramAccountManageService.insertAcquisitionProgramAccountManage(acquisitionProgramAccountManage));
    }

    /**
     * 修改软件账户管理
     */
    @PreAuthorize("@ss.hasPermi('acquisition:accountManage:edit')")
    @Log(title = "软件账户管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AcquisitionProgramAccountManage acquisitionProgramAccountManage)
    {
        return toAjax(acquisitionProgramAccountManageService.updateAcquisitionProgramAccountManage(acquisitionProgramAccountManage));
    }

    /**
     * 删除软件账户管理
     */
    @PreAuthorize("@ss.hasPermi('acquisition:accountManage:remove')")
    @Log(title = "软件账户管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(acquisitionProgramAccountManageService.deleteAcquisitionProgramAccountManageByIds(ids));
    }
}
