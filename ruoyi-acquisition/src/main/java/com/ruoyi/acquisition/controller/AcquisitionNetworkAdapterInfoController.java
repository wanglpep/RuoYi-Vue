package com.ruoyi.acquisition.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.acquisition.domain.vo.AcquisitionNetworkAdapterInfoVo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.acquisition.domain.AcquisitionNetworkAdapterInfo;
import com.ruoyi.acquisition.service.IAcquisitionNetworkAdapterInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 主机程序网卡信息Controller
 *
 * @author abner
 * @date 2024-03-02
 */
@RestController
@RequestMapping("/acquisition/info")
public class AcquisitionNetworkAdapterInfoController extends BaseController {
    @Autowired
    private IAcquisitionNetworkAdapterInfoService acquisitionNetworkAdapterInfoService;

    /**
     * 查询主机程序网卡信息列表
     */
    @PreAuthorize("@ss.hasPermi('acquisition:info:list')")
    @GetMapping("/list")
    public TableDataInfo list(AcquisitionNetworkAdapterInfo acquisitionNetworkAdapterInfo) {
        startPage();
        List<AcquisitionNetworkAdapterInfo> list = acquisitionNetworkAdapterInfoService.selectAcquisitionNetworkAdapterInfoList(acquisitionNetworkAdapterInfo);
        return getDataTable(list);
    }

    /**
     * 导出主机程序网卡信息列表
     */
    @PreAuthorize("@ss.hasPermi('acquisition:info:export')")
    @Log(title = "主机程序网卡信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, AcquisitionNetworkAdapterInfoVo acquisitionNetworkAdapterInfo) {
        List<AcquisitionNetworkAdapterInfoVo> list = acquisitionNetworkAdapterInfoService.selectAcquisitionNetworkAdapterInfoVoList(acquisitionNetworkAdapterInfo);
        ExcelUtil<AcquisitionNetworkAdapterInfoVo> util = new ExcelUtil<>(AcquisitionNetworkAdapterInfoVo.class);
        util.exportExcel(response, list, "主机程序网卡信息数据");
    }

    /**
     * 获取主机程序网卡信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('acquisition:info:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return success(acquisitionNetworkAdapterInfoService.selectAcquisitionNetworkAdapterInfoById(id));
    }

    /**
     * 新增主机程序网卡信息
     */
    @PreAuthorize("@ss.hasPermi('acquisition:info:add')")
    @Log(title = "主机程序网卡信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AcquisitionNetworkAdapterInfo acquisitionNetworkAdapterInfo) {
        return toAjax(acquisitionNetworkAdapterInfoService.insertAcquisitionNetworkAdapterInfo(acquisitionNetworkAdapterInfo));
    }

    /**
     * 修改主机程序网卡信息
     */
    @PreAuthorize("@ss.hasPermi('acquisition:info:edit')")
    @Log(title = "主机程序网卡信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AcquisitionNetworkAdapterInfo acquisitionNetworkAdapterInfo) {
        return toAjax(acquisitionNetworkAdapterInfoService.updateAcquisitionNetworkAdapterInfo(acquisitionNetworkAdapterInfo));
    }

    /**
     * 删除主机程序网卡信息
     */
    @PreAuthorize("@ss.hasPermi('acquisition:info:remove')")
    @Log(title = "主机程序网卡信息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(acquisitionNetworkAdapterInfoService.deleteAcquisitionNetworkAdapterInfoByIds(ids));
    }


    /**
     * 查询主机程序网卡信息列表
     */
    @PreAuthorize("@ss.hasPermi('acquisition:info:list')")
    @GetMapping("/listHostInfo")
    public TableDataInfo listHostInfo(AcquisitionNetworkAdapterInfoVo acquisitionNetworkAdapterInfo) {
        startPage();
        List<AcquisitionNetworkAdapterInfoVo> list = acquisitionNetworkAdapterInfoService.selectAcquisitionNetworkAdapterInfoVoList(acquisitionNetworkAdapterInfo);
        return getDataTable(list);
    }
}
