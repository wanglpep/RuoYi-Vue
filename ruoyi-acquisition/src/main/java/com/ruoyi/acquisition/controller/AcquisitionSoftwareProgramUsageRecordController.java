package com.ruoyi.acquisition.controller;

import com.ruoyi.acquisition.domain.AcquisitionSoftwareProgramUsageRecord;
import com.ruoyi.acquisition.domain.vo.AcquisitionSoftwareProgramUsageRecordVo;
import com.ruoyi.acquisition.service.IAcquisitionSoftwareProgramUsageRecordService;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 软件程序使用记录Controller
 *
 * @author abner
 * @date 2024-03-02
 */
@RestController
@RequestMapping("/acquisition/record")
public class AcquisitionSoftwareProgramUsageRecordController extends BaseController {
    @Autowired
    private IAcquisitionSoftwareProgramUsageRecordService acquisitionSoftwareProgramUsageRecordService;

    /**
     * 查询软件程序使用记录列表
     */
    @PreAuthorize("@ss.hasPermi('acquisition:record:list')")
    @GetMapping("/list")
    public TableDataInfo list(AcquisitionSoftwareProgramUsageRecord acquisitionSoftwareProgramUsageRecord) {
        startPage();
        List<AcquisitionSoftwareProgramUsageRecord> list = acquisitionSoftwareProgramUsageRecordService.selectAcquisitionSoftwareProgramUsageRecordList(acquisitionSoftwareProgramUsageRecord);
        return getDataTable(list);
    }

    /**
     * 导出软件程序使用记录列表
     */
    @PreAuthorize("@ss.hasPermi('acquisition:record:export')")
    @Log(title = "软件程序使用记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, AcquisitionSoftwareProgramUsageRecordVo acquisitionSoftwareProgramUsageRecord) {
        List<AcquisitionSoftwareProgramUsageRecordVo> list = acquisitionSoftwareProgramUsageRecordService.selectAcquisitionSoftwareProgramUsageRecordVoList(acquisitionSoftwareProgramUsageRecord);
        ExcelUtil<AcquisitionSoftwareProgramUsageRecordVo> util = new ExcelUtil<>(AcquisitionSoftwareProgramUsageRecordVo.class);
        util.exportExcel(response, list, "软件程序使用记录数据");
    }

    /**
     * 获取软件程序使用记录详细信息
     */
    @PreAuthorize("@ss.hasPermi('acquisition:record:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return success(acquisitionSoftwareProgramUsageRecordService.selectAcquisitionSoftwareProgramUsageRecordById(id));
    }

    /**
     * 新增软件程序使用记录
     */
    @PreAuthorize("@ss.hasPermi('acquisition:record:add')")
    @Log(title = "软件程序使用记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AcquisitionSoftwareProgramUsageRecord acquisitionSoftwareProgramUsageRecord) {
        return toAjax(acquisitionSoftwareProgramUsageRecordService.insertAcquisitionSoftwareProgramUsageRecord(acquisitionSoftwareProgramUsageRecord));
    }

    /**
     * 修改软件程序使用记录
     */
    @PreAuthorize("@ss.hasPermi('acquisition:record:edit')")
    @Log(title = "软件程序使用记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AcquisitionSoftwareProgramUsageRecord acquisitionSoftwareProgramUsageRecord) {
        return toAjax(acquisitionSoftwareProgramUsageRecordService.updateAcquisitionSoftwareProgramUsageRecord(acquisitionSoftwareProgramUsageRecord));
    }

    /**
     * 删除软件程序使用记录
     */
    @PreAuthorize("@ss.hasPermi('acquisition:record:remove')")
    @Log(title = "软件程序使用记录", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(acquisitionSoftwareProgramUsageRecordService.deleteAcquisitionSoftwareProgramUsageRecordByIds(ids));
    }

    /**
     * 查询软件程序使用记录列表
     */
    @PreAuthorize("@ss.hasPermi('acquisition:record:list')")
    @GetMapping("/listHostInfo")
    public TableDataInfo listHostInfo(AcquisitionSoftwareProgramUsageRecordVo acquisitionSoftwareProgramUsageRecord) {
        startPage();
        List<AcquisitionSoftwareProgramUsageRecordVo> list = acquisitionSoftwareProgramUsageRecordService.selectAcquisitionSoftwareProgramUsageRecordVoList(acquisitionSoftwareProgramUsageRecord);
        return getDataTable(list);
    }
}
