package com.ruoyi.acquisition.controller;

import com.alibaba.fastjson2.JSON;
import com.ruoyi.acquisition.config.ConfigInfo;
import com.ruoyi.acquisition.domain.AcquisitionProgramAccountManage;
import com.ruoyi.acquisition.domain.vo.ApiAcquisitionHostProgramVo;
import com.ruoyi.acquisition.service.IAcquisitionHostProgramDataApi;
import com.ruoyi.acquisition.service.IAcquisitionProgramAccountManageService;
import com.ruoyi.common.annotation.Anonymous;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 数据采集程序
 *
 * @author wanglp
 * @create 2024/3/2 23:27
 */
@RestController
@RequestMapping("/api/acquisition/program")
@Slf4j
public class AcquisitionHostProgramDataApiController extends BaseController {
    public static final String PARAMETERS = "Parameters";
    @Autowired
    private IAcquisitionHostProgramDataApi acquisitionHostProgramDataApi;
    @Autowired
    private IAcquisitionProgramAccountManageService acquisitionProgramAccountManageService;
    @Resource
    private ConfigInfo configInfo;

    /**
     * 接收程序信息
     *
     * @param acquisitionHostProgramMap 参数
     * @param request                   http
     * @return AjaxResult
     */
    @Anonymous
    @PostMapping(value = "/receiveProgramInformation")
    public AjaxResult receiveProgramInformation(@RequestBody Map<String, List<ApiAcquisitionHostProgramVo>> acquisitionHostProgramMap, HttpServletRequest request) {
        AjaxResult result = acquisitionHostProgramDataApi.checkReceiveParams(acquisitionHostProgramMap, request, configInfo.getHostProgramInfo());
        if (result.isError()) {
            log.error("receiveProgramInformation 请求失败，serviceCode:{},acquisitionHostProgramMap:{}", configInfo.getHostProgramInfo().getServiceCode(), JSON.toJSONString(acquisitionHostProgramMap));
            return result;
        }
        try {
            result = acquisitionHostProgramDataApi.insertAcquisitionHostProgramVo(acquisitionHostProgramMap.get(PARAMETERS));
        } catch (Exception e) {
            log.error("receiveProgramInformation 存放信息时错误,serviceCode:{},acquisitionHostProgramMap:{}", configInfo.getHostProgramInfo().getServiceCode(), JSON.toJSONString(acquisitionHostProgramMap), e);
            result = AjaxResult.error("存放信息时错误，" + e.getMessage());
        }
        return result;
    }

    /**
     * 查询软件账户信息
     *
     * @param accountManage 参数
     * @param request       http
     * @return
     */
    @Anonymous
    @PostMapping(value = "/queryAccountManage")
    public AjaxResult queryAccountManage(@RequestBody AcquisitionProgramAccountManage accountManage, HttpServletRequest request) {
        AjaxResult result = acquisitionHostProgramDataApi.checkReceiveParamsAccountManage(accountManage, request, configInfo.getAccountManage());
        if (result.isError()) {
            log.error("accountManage 请求失败，serviceCode:{},acquisitionHostProgramMap:{}", configInfo.getHostProgramInfo().getServiceCode(), JSON.toJSONString(accountManage));
            return result;
        }
        List<AcquisitionProgramAccountManage> list = acquisitionProgramAccountManageService.selectAcquisitionProgramAccountManageList(accountManage);
        List<Map<String, Object>> convert = Optional.ofNullable(list).orElse(new ArrayList<>()).stream().map(bean -> {
            Map<String, Object> obj = new HashMap<>();
            obj.put("id", bean.getId());
            obj.put("loginAccount", bean.getLoginAccount());
            obj.put("createTime", bean.getCreateTime());
            obj.put("programName", bean.getProgramName());
            return obj;
        }).collect(Collectors.toList());
        return AjaxResult.success("查询成功", convert);
    }
}
