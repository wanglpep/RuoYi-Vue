package com.ruoyi.acquisition.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.acquisition.domain.AcquisitionHostProgram;
import com.ruoyi.acquisition.service.IAcquisitionHostProgramService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 主机程序Controller
 * 
 * @author abner
 * @date 2024-03-02
 */
@RestController
@RequestMapping("/acquisition/program")
public class AcquisitionHostProgramController extends BaseController
{
    @Autowired
    private IAcquisitionHostProgramService acquisitionHostProgramService;

    /**
     * 查询主机程序列表
     */
    @PreAuthorize("@ss.hasPermi('acquisition:program:list')")
    @GetMapping("/list")
    public TableDataInfo list(AcquisitionHostProgram acquisitionHostProgram)
    {
        startPage();
        List<AcquisitionHostProgram> list = acquisitionHostProgramService.selectAcquisitionHostProgramList(acquisitionHostProgram);
        return getDataTable(list);
    }

    /**
     * 导出主机程序列表
     */
    @PreAuthorize("@ss.hasPermi('acquisition:program:export')")
    @Log(title = "主机程序", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, AcquisitionHostProgram acquisitionHostProgram)
    {
        List<AcquisitionHostProgram> list = acquisitionHostProgramService.selectAcquisitionHostProgramList(acquisitionHostProgram);
        ExcelUtil<AcquisitionHostProgram> util = new ExcelUtil<AcquisitionHostProgram>(AcquisitionHostProgram.class);
        util.exportExcel(response, list, "主机程序数据");
    }

    /**
     * 获取主机程序详细信息
     */
    @PreAuthorize("@ss.hasPermi('acquisition:program:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(acquisitionHostProgramService.selectAcquisitionHostProgramById(id));
    }

    /**
     * 新增主机程序
     */
    @PreAuthorize("@ss.hasPermi('acquisition:program:add')")
    @Log(title = "主机程序", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody AcquisitionHostProgram acquisitionHostProgram)
    {
        return toAjax(acquisitionHostProgramService.insertAcquisitionHostProgram(acquisitionHostProgram));
    }

    /**
     * 修改主机程序
     */
    @PreAuthorize("@ss.hasPermi('acquisition:program:edit')")
    @Log(title = "主机程序", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody AcquisitionHostProgram acquisitionHostProgram)
    {
        return toAjax(acquisitionHostProgramService.updateAcquisitionHostProgram(acquisitionHostProgram));
    }

    /**
     * 删除主机程序
     */
    @PreAuthorize("@ss.hasPermi('acquisition:program:remove')")
    @Log(title = "主机程序", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(acquisitionHostProgramService.deleteAcquisitionHostProgramByIds(ids));
    }
}
