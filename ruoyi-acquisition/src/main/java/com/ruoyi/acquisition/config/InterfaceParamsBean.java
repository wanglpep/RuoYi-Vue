package com.ruoyi.acquisition.config;

import lombok.Data;

/**
 * 接口参数
 *
 * @author wanglp
 * @create 2024/3/2 23:51
 */
@Data
public class InterfaceParamsBean {

    /**
     * 接口服务编码
     */
    private String serviceCode;

    /**
     * 接口服务名称
     */
    private String serviceName;

    /**
     * 认证key
     */
    private String openApiSign;

    private boolean enabled = false;

}
