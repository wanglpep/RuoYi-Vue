package com.ruoyi.acquisition.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author wanglp
 * @create 2024/3/2 23:48
 */
@Component
@ConfigurationProperties(prefix = "intf.acquisition")
@Data
public class ConfigInfo {

    private InterfaceParamsBean hostProgramInfo;
    private InterfaceParamsBean accountManage;

}
