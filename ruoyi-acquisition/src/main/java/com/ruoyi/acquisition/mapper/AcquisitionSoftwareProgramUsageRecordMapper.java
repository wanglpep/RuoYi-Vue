package com.ruoyi.acquisition.mapper;

import java.util.List;
import com.ruoyi.acquisition.domain.AcquisitionSoftwareProgramUsageRecord;
import com.ruoyi.acquisition.domain.vo.AcquisitionSoftwareProgramUsageRecordVo;

/**
 * 软件程序使用记录Mapper接口
 *
 * @author abner
 * @date 2024-03-02
 */
public interface AcquisitionSoftwareProgramUsageRecordMapper
{
    /**
     * 查询软件程序使用记录
     *
     * @param id 软件程序使用记录主键
     * @return 软件程序使用记录
     */
    public AcquisitionSoftwareProgramUsageRecord selectAcquisitionSoftwareProgramUsageRecordById(Long id);

    /**
     * 查询软件程序使用记录列表
     *
     * @param acquisitionSoftwareProgramUsageRecord 软件程序使用记录
     * @return 软件程序使用记录集合
     */
    public List<AcquisitionSoftwareProgramUsageRecord> selectAcquisitionSoftwareProgramUsageRecordList(AcquisitionSoftwareProgramUsageRecord acquisitionSoftwareProgramUsageRecord);

    /**
     * 新增软件程序使用记录
     *
     * @param acquisitionSoftwareProgramUsageRecord 软件程序使用记录
     * @return 结果
     */
    public int insertAcquisitionSoftwareProgramUsageRecord(AcquisitionSoftwareProgramUsageRecord acquisitionSoftwareProgramUsageRecord);

    /**
     * 修改软件程序使用记录
     *
     * @param acquisitionSoftwareProgramUsageRecord 软件程序使用记录
     * @return 结果
     */
    public int updateAcquisitionSoftwareProgramUsageRecord(AcquisitionSoftwareProgramUsageRecord acquisitionSoftwareProgramUsageRecord);

    /**
     * 删除软件程序使用记录
     *
     * @param id 软件程序使用记录主键
     * @return 结果
     */
    public int deleteAcquisitionSoftwareProgramUsageRecordById(Long id);

    /**
     * 批量删除软件程序使用记录
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteAcquisitionSoftwareProgramUsageRecordByIds(Long[] ids);

    List<AcquisitionSoftwareProgramUsageRecordVo> selectAcquisitionSoftwareProgramUsageRecordVoList(AcquisitionSoftwareProgramUsageRecordVo acquisitionSoftwareProgramUsageRecord);
}
