-- 创建数据库
#drop database software_monitoring;
#create database software_monitoring default character set utf8;

CREATE TABLE `acquisition_host_program`
(
    `id`                      bigint UNSIGNED AUTO_INCREMENT PRIMARY KEY COMMENT '唯一标识符',
    `host_name`               VARCHAR(300)           NOT NULL COMMENT '主机名称',
    `system_version`          VARCHAR(100)           NOT NULL COMMENT '系统版本',
    `program_path`            VARCHAR(512)           NOT NULL COMMENT '程序路径',
    `program_name`            VARCHAR(255)           NOT NULL COMMENT '程序名称',
    `program_file_version`    VARCHAR(100)           NOT NULL COMMENT '程序文件版本',
    `program_product_version` VARCHAR(100)           NOT NULL COMMENT '程序产品版本',
    `create_by`               varchar(64) default '' null comment '创建者',
    `create_time`             datetime               null comment '创建时间',
    `update_by`               varchar(64) default '' null comment '更新者',
    `update_time`             datetime               null comment '更新时间',
    UNIQUE KEY `unique_host_program` (`host_name`, `program_name`, `program_file_version`,
                                      `program_product_version`) COMMENT '确保主机上的程序唯一性'
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_general_ci COMMENT ='主机程序表';
CREATE TABLE `acquisition_software_program_usage_record`
(
    `id`                 bigint UNSIGNED AUTO_INCREMENT PRIMARY KEY COMMENT '唯一标识符',
    `host_program_id`    bigint                 not null comment '主机程序表ID',
    `program_start_time` DATETIME               NOT NULL COMMENT '程序启动时间',
    `last_run_time`      DATETIME               NULL COMMENT '最后运行时间',
    `login_account`      VARCHAR(255)           NULL COMMENT '登录账号',
    `login_time`         DATETIME               NULL COMMENT '登录时间',
    `process_pid`        VARCHAR(10)            NOT NULL COMMENT '进程PID',
    `create_by`          varchar(64) default '' null comment '创建者',
    `create_time`        datetime               null comment '创建时间',
    `update_by`          varchar(64) default '' null comment '更新者',
    `update_time`        datetime               null comment '更新时间'
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_general_ci COMMENT ='软件程序使用记录';
CREATE TABLE `acquisition_network_adapter_info`
(
    `id`                      bigint UNSIGNED AUTO_INCREMENT PRIMARY KEY COMMENT '唯一标识符',
    `program_usage_record_id` bigint                 not null comment '软件程序使用记录表ID',
    `host_program_id`         bigint                 not null comment '主机程序表ID',
    `mac_address`             VARCHAR(50)            NOT NULL COMMENT 'MAC地址',
    `ip_address`              VARCHAR(50)            NOT NULL COMMENT 'IP地址',
    `create_by`               varchar(64) default '' null comment '创建者',
    `create_time`             datetime               null comment '创建时间',
    `update_by`               varchar(64) default '' null comment '更新者',
    `update_time`             datetime               null comment '更新时间'
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_general_ci COMMENT ='主机程序网卡信息表';

CREATE TABLE `acquisition_program_account_manage`
(
    `id`               bigint UNSIGNED AUTO_INCREMENT PRIMARY KEY COMMENT '唯一标识符',
    `program_name`     VARCHAR(255)           NOT NULL COMMENT '程序名称',
    `login_account`    VARCHAR(255)           NOT NULL COMMENT '登录账号',
    `account_password` VARCHAR(255)           NULL COMMENT '账号密码',
    `create_by`        varchar(64) default '' null comment '创建者',
    `create_time`      datetime               null comment '创建时间',
    `update_by`        varchar(64) default '' null comment '更新者',
    `update_time`      datetime               null comment '更新时间',
    UNIQUE KEY `unique_program_name` (`program_name`, `login_account`) COMMENT '确保主机上的程序唯一性'
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  COLLATE = utf8_general_ci COMMENT ='软件账户管理表';



