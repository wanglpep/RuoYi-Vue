-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('主机程序网卡信息', '3', '1', 'info', 'acquisition/info/index', 1, 0, 'C', '0', '0', 'acquisition:info:list', '#', 'admin', sysdate(), '', null, '主机程序网卡信息菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('主机程序网卡信息查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'acquisition:info:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('主机程序网卡信息新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'acquisition:info:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('主机程序网卡信息修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'acquisition:info:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('主机程序网卡信息删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'acquisition:info:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('主机程序网卡信息导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'acquisition:info:export',       '#', 'admin', sysdate(), '', null, '');