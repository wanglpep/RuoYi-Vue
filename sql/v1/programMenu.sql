-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('主机程序', '3', '1', 'program', 'acquisition/program/index', 1, 0, 'C', '0', '0', 'acquisition:program:list', '#', 'admin', sysdate(), '', null, '主机程序菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('主机程序查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'acquisition:program:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('主机程序新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'acquisition:program:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('主机程序修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'acquisition:program:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('主机程序删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'acquisition:program:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('主机程序导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'acquisition:program:export',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('主机程序使用记录', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'acquisition:program:record',       '#', 'admin', sysdate(), '', null, '');
