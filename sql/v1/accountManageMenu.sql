-- 菜单 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('软件账户管理', '2019', '1', 'accountManage', 'acquisition/accountManage/index', 1, 0, 'C', '0', '0', 'acquisition:accountManage:list', '#', 'admin', sysdate(), '', null, '软件账户管理菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('软件账户管理查询', @parentId, '1',  '#', '', 1, 0, 'F', '0', '0', 'acquisition:accountManage:query',        '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('软件账户管理新增', @parentId, '2',  '#', '', 1, 0, 'F', '0', '0', 'acquisition:accountManage:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('软件账户管理修改', @parentId, '3',  '#', '', 1, 0, 'F', '0', '0', 'acquisition:accountManage:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('软件账户管理删除', @parentId, '4',  '#', '', 1, 0, 'F', '0', '0', 'acquisition:accountManage:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, path, component, is_frame, is_cache, menu_type, visible, status, perms, icon, create_by, create_time, update_by, update_time, remark)
values('软件账户管理导出', @parentId, '5',  '#', '', 1, 0, 'F', '0', '0', 'acquisition:accountManage:export',       '#', 'admin', sysdate(), '', null, '');